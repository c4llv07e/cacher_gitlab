
# Cacher snippet dumper

Dumps all snippets for the cacher to the gitlab snippet store.

## Usage

```shell
CACHER_API_KEY="key" CACHER_API_TOKEN="token" GITLAB_TOKEN="token" python3 ./main.py
```

You can also specify `GITLAB_URL` to use different gitlab instance, example:

```shell
GITLAB_URL=http://my_gitlab.com/
```

`GITLAB_PROJECT` controls which project snippets will be dumped to. By default,
it dumps them to the personal snippets.

```shell
GITLAB_PROJECT=c4llv07e/cacher_gitlab
```

## P.S.

It was written in, like, an hour, for personal use once. This is not an example
of good code, read the gitlab docs for this.
