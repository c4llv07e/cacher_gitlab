#!/usr/bin/python
import requests
import os
from urllib.parse import quote_plus as url_encode

def env_get_or_exit(val):
    if val not in os.environ:
        print(f"error, {val} is unset")
        exit(-1)
        return None
    return os.environ[val]

cacher_api_key = env_get_or_exit("CACHER_API_KEY")
cacher_api_token = env_get_or_exit("CACHER_API_TOKEN")
gitlab_token = env_get_or_exit("GITLAB_TOKEN")
gitlab_project = os.environ.get("GITLAB_PROJECT", None)

class Snippet_file:
    def __init__(self, filename, content):
        self.filename = filename
        self.content = content
        pass
    def __str__(self):
        return (f"file: \"{self.filename}\":\n"+
                self.content + "\n")
    def __repr__(self):
        return self.__str__()
    pass

class Label:
    def __init__(self, title, guids):
        self.title = title
        self.guids = guids
        pass
    def __str__(self):
        return f"label {self.title}: {self.guids}"
    def __repr__(self):
        return self.__str__()
    pass

class Snippet:
    def __init__(self, title, description, files, is_private):
        self.title = title
        self.description = description
        self.files = files
        self.is_private = is_private
        pass
    def __str__(self):
        return (f"snippet: \"{self.title}\" \"{self.description}\"\n" +
                str(self.files))
    def __repr__(self):
        return self.__str__()
    pass

def parse_snippets(library):
    lib_labels = list()
    json_labels = library["labels"]
    for json_label in json_labels:
        title = json_label["title"]
        guids = list(x["guid"] for x in json_label["snippets"])
        label = Label(title, guids)
        lib_labels.append(label)
        pass
    lib_snippets = list()
    json_snippets = library["snippets"]
    for snippet in json_snippets:
        lib_files = list()
        title = snippet["title"]
        guid = snippet["guid"]
        is_private = snippet["isPrivate"]
        labels = list(filter(lambda label: guid in label.guids, lib_labels))
        labels_titles = ", ".join(list(map(lambda x: x.title, labels)))
        description = snippet["description"]
        lib_description = "[" + labels_titles + "]" + (("\n" + description) if description else "")
        snippet_files = snippet["files"]
        for snippet_file in snippet_files:
            filename = snippet_file["filename"]
            content = snippet_file["content"]
            lib_file = Snippet_file(filename, content)
            lib_files.append(lib_file)
            pass
        lib_snippet = Snippet(title, lib_description, lib_files, is_private)
        lib_snippets.append(lib_snippet)
        pass
    return lib_snippets

def generate_payload(lib_snippet):
    return {
        "title": lib_snippet.title,
        "description": lib_snippet.description,
        "visibility": "private" if lib_snippet.is_private else "public",
        "files": [
            {
                "file_path": sn_file.filename,
                "content": sn_file.content,
            } for sn_file in lib_snippet.files
        ],
    }

cacher_url = "https://api.cacher.io/"
cacher_headers = {
    'X-Api-Key': cacher_api_key,
    'X-Api-Token': cacher_api_token,
    'cache-control': 'no-cache',
}

gitlab_url = os.environ.get("GITLAB_URL", "https://gitlab.com")
if gitlab_url[-1] != "/":
    gitlab_url += "/"
gitlab_url += "api/v4/"
gitlab_headers = {
    "PRIVATE-TOKEN": gitlab_token,
    "Content-Type": "application/json",
}

resp = requests.get(cacher_url+"integrations/show_all", headers=cacher_headers)
json = resp.json()

def gitlab_upload(name, snippet):
    payload = generate_payload(snippet)
    if gitlab_project is None:
        uri_path = "snippets"
        pass
    else:
        uri_path = "projects/" + url_encode(gitlab_project) + "/snippets"
        pass
    resp = requests.post(gitlab_url+uri_path, headers=gitlab_headers, json=payload)
    if resp.status_code // 100 == 2:
        print(f"\"{name}\"'s {snippet.title} has been added")
        pass
    else:
        print(f"can't add \"{name}\"'s \"{snippet.title}\"")
        print(resp.text)
        pass
    pass

personal_library = json["personalLibrary"]
snippets = parse_snippets(personal_library)
for snippet in snippets:
    gitlab_upload("personal", snippet)
    pass

json_teams = json["teams"]
for json_team in json_teams:
    team_name = json_team["name"]
    library = json_team["library"]
    team_snippets = parse_snippets(library)
    for snippet in team_snippets:
        gitlab_upload(team_name, snippet)
        pass
    pass
